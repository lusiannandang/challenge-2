function checkEmail(email) {
  let regex = /^[^ ]+@[^ ]+\.[a-z]{2,3}$/;

  if (email != null) {
    if (typeof email == "string") {
      if (email.match(regex)) {
        return "Valid";
      } else {
        let regex2 = new RegExp("[a-z0-9]+@binar");
        if (regex2.test(email)) {
          return "INVALID";
        } else {
          return "Error : Alamat email tidak Valid";
        }
      }
    } else {
      return "Error : Invalid Data Type";
    }
  } else {
    return "Error: Bro where is the parameter";
  }
}

console.log(checkEmail("apranata@binar.co.id"));
console.log(checkEmail("apranata@binar.com"));
console.log(checkEmail("apranata@binar"));
console.log(checkEmail("apranata"));
// console.log(checkTypeNumber(checkEmail(3322)));
console.log(checkEmail());
