function isValidPassword(password) {
  var lowerCaseLetter = /[a-z]/g;
  var upperCaseLetter = /[A-Z]/g;
  var number = /[0-9]/g;
  if (password != null) {
    if (typeof password == "string") {
      if (password.length >= 8) {
        if (password.match(lowerCaseLetter)) {
          if (password.match(upperCaseLetter)) {
            if (password.match(number)) {
              return "True";
            } else {
              return "Kurang angka";
            }
          } else {
            return "Kurang huruf besar";
          }
        } else {
          return "Kurang huruf kecil";
        }
      } else {
        return "Tidak boleh kurang dari 8";
      }
    } else {
      return "Pakein huruf yaa";
    }
  } else {
    return "Tidak ada parameter";
  }
}

console.log(isValidPassword("Meong2021"));
console.log(isValidPassword("meong2021"));
console.log(isValidPassword("@eong"));
console.log(isValidPassword("Meong2"));
console.log(isValidPassword(0));
console.log(isValidPassword());
