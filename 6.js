const dataAngka = [9, 4, 7, 4, 3, 2, 2, 8];

function getAngkaTerbesar(angka) {
  if (angka != 0) {
    if (angka != null) {
      dataAngka.sort(function (a, b) {
        return b - a;
      });
      const terbesar = dataAngka[1];
      return terbesar;
    } else {
      return "Error";
    }
  } else {
    return "Itu isinya 0";
  }
}

console.log(getAngkaTerbesar(dataAngka));
console.log(getAngkaTerbesar(0));
console.log(getAngkaTerbesar());
