const dataPenjualanNovel = [
  {
    idProduct: "BOOK002421",
    namaProduk: "Pulang - Pergi",
    penulis: "Tere Liye",
    hargaBeli: 60000,
    hargaJual: 86000,
    totalTerjual: 150,
    sisaStok: 17,
  },
  {
    idProduct: "BOOK002351",
    namaProduk: "Selamat Tinggal",
    penulis: "Tere Liye",
    hargaBeli: 75000,
    hargaJual: 103000,
    totalTerjual: 171,
    sisaStok: 20,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Garis Waktu",
    penulis: "Fiersa Besari",
    hargaBeli: 67000,
    hargaJual: 99000,
    totalTerjual: 213,
    sisaStok: 5,
  },
  {
    idProduct: "BOOK002941",
    namaProduk: "Laskar Pelangi",
    penulis: "Andrea Hirata",
    hargaBeli: 55000,
    hargaJual: 68000,
    totalTerjual: 20,
    sisaStok: 56,
  },
];

function getInfoPenjualan(dataPenjualan) {
  //totalKeuntungan
  const totalKeuntungan = dataPenjualan.map((dataPenjualanNovel) => (dataPenjualanNovel.hargaJual - dataPenjualanNovel.hargaBeli) * dataPenjualanNovel.totalTerjual).reduce((prev, curr) => prev + curr, 0);

  const totalModal = dataPenjualan.map((dataPenjualanNovel) => (dataPenjualanNovel.sisaStok + dataPenjualanNovel.totalTerjual) * dataPenjualanNovel.hargaBeli).reduce((prev, curr) => prev + curr, 0);

  //presentaseKeuntungan
  const presentaseKeuntungan = (totalKeuntungan / totalModal) * 100;
  function rupiah(uang) {
    var reverse = uang.toString().split("").reverse().join(""),
      ribuan = reverse.match(/\d{1,3}/g);
    ribuan = ribuan.join(".").split("").reverse().join("");
    return ribuan;
  }

  //Produk terlaris
  const sorting = dataPenjualanNovel.sort((a, b) => b.totalTerjual - a.totalTerjual);
  produkterlaris = sorting[0].namaProduk;

  //Penulis terlaris
  const penulisTerlaris = {};
  dataPenjualan.forEach((element) => {
    if (element.penulis in penulisTerlaris) {
      penulisTerlaris[element.penulis] = penulisTerlaris[element.penulis] + element.totalTerjual;
    } else {
      penulisTerlaris[`${element.penulis}`] = element.totalTerjual;
    }
  });
  let mostPopularWriterCount = Math.max(...Object.values(penulisTerlaris));
  const getKeyByValue = (object, value) => Object.keys(object).find((key) => object[key] === value);

  const data = {
    totalKeuntungan: "RP." + rupiah(totalKeuntungan),
    totalModal: "RP." + rupiah(totalModal),
    presentaseKeuntungan: `${presentaseKeuntungan}%`,
    produkBukuTerlaris: produkterlaris,
    penulisTerlaris: getKeyByValue(penulisTerlaris, mostPopularWriterCount),
  };
  return data;
}

console.log(getInfoPenjualan(dataPenjualanNovel));
