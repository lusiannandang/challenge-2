function getSplitName(getnama) {
  if (typeof getnama == "string") {
    let nama = getnama.split(" ");
    let result = { firstName: null, middleName: null, lastName: null };

    if (nama.length === 1) {
      result.firstName = nama[0];
      return result;
    } else if (nama.length === 2) {
      result.firstName = nama[0];
      result.lastName = nama[1];
      return result;
    } else if (nama.length === 3) {
      result.firstName = nama[0];
      result.middleName = nama[1];
      result.lastName = nama[2];
      return result;
    } else {
      return "Hanya menerima 3 karakter nama";
    }
  } else {
    return "Harus String";
  }
}

console.log(getSplitName("Aldi Daniela Pranata"));
console.log(getSplitName("Dwi Kuncoro"));
console.log(getSplitName("Aurora"));
console.log(getSplitName("Aurora Aureliya Sukma Darma"));
console.log(getSplitName(0));
